const scraper = require('./pageScraper');

async function scrapeAll(browserInstance){
    let browser;
    try{
        browser = await browserInstance;
        await scraper.scraper(browser);
    }
    catch(err){
        console.log("Broeser Instance error");
    }
}

module.exports = (browserInstance)=> scrapeAll(browserInstance);