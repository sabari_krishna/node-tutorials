const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes/routes');

mongoose.Promise = global.Promise;

const app = express();
const port = 8000
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use('/routes',routes);

app.set("views", "./views");
app.set("view-engine", "ejs");


const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/Data?retryWrites=true&w=majority";

mongoose.connect(url,{
    useNewUrlParser:true
}).then(()=>{
    console.log("connected to DB");
})

app.listen(port,()=>{
    console.log(`listening at ${port}`);
});
