const mongosse = require('mongoose');

const empSchema = mongosse.Schema({
    Name:String,
    Designation:String,
    Id:Number,
    DOJ:String
})

module.exports = mongosse.model("employee",empSchema);