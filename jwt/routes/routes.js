const express = require('express');
const bcrypt = require('bcrypt');
const app = express();
const User = require('../model/model');
const jwt = require('jsonwebtoken');
const auth = require('../auth');
const fs = require('fs');
const path = require('path');
app.set('view-engine','ejs');

var privateKey = fs.readFileSync(path.resolve('./routes/key/private.key'),'utf8');
// console.log(privateKey)
// var publicKey = fs.readFileSync(path.resolve('./routes/key/public.key'));

const router = express.Router();

router.get('/signup',(req,res)=>{
    res.render('signup.ejs');
})

router.get('/login',(req,res)=>{
    res.render('login.ejs');
})

router.post('/register',async (req,res)=>{
    console.log(req.body)
    const email = req.body.email;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password,salt); 
    const user = new User({
        email: req.body.email,
        password: hashedPassword
    })
    try{
        User.findOne({email}).then((data)=>{
            if(data == null){
                user.save()
                .then((data)=>{
                    res.send({
                        code:200,
                        msg:"User created successfully"
                    })
                })
            }
            return res.status(400).json({
                message: "user already exist"
            })
        })
      
    }catch(err){
        console.log(err)
    }
})

router.post('/login',(req,res)=>{
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({email: email}).then(async(data)=>{
        console.log(data)
        const pass =  await bcrypt.compare(password, data.password);
        console.log(pass)
        if(pass){
            const payload = {
                id: data.email
            }
            jwt.sign(payload,privateKey,{ algorithm: 'RS256', expiresIn:100 },(err,token)=>{
                if(err){
                    res.send(err)
                }
                res.status(200).json({
                    token
                });
            })
        }
    })
})
router.get('/id',auth,(req,res)=>{
    console.log(req.user.id);
    const id = req.user.id;
    User.find()
    .then((data)=>{
        res.send(data);
    })
    
    
})

module.exports = router;