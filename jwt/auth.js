const jwt = require("jsonwebtoken");
const fs = require('fs');
const path = require('path');

// var publicKey = fs.readFileSync(path.resolve('./key/private.key'));
var publicKey = fs.readFileSync(path.resolve('./routes/key/public.key'),'utf8');

module.exports = function(req, res, next) {
  // console.log("Testfghj")
  const token = req.header("token");
  if (!token) return res.status(401).json({ message: "Auth Error" });

  try {
    const decoded = jwt.verify(token, publicKey,{ algorithms: 'RS256'});

    console.log('---------------->',decoded)
    req.user = decoded;
    // console.log("---------->",req.user);
    next();
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: "Invalid Token" });
  }
};