const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const router = require('./routes/routes');


const app = express();
app.set('view-engine','ejs');
app.use(bodyparser.urlencoded({extended:true}));
app.use(express.json());

app.use('/routes',router);

mongoose.Promise = global.Promise;
const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/JWT?retryWrites=true&w=majority";

mongoose.connect(url,{
    useNewUrlParser:true
}).then(()=>{
    console.log("connected to db");
})


app.listen('8081',()=>{
    console.log("Listening at 8081");
})