const mongoose = require('mongoose');

const  mailSchema = mongoose.Schema({
    mail: String,
    name: String,
});

module.exports = mongoose.model("mail",mailSchema);