const express = require("express");
const mailer = require("nodemailer");
const cron = require("node-cron");
const model = require("./model/model");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const app = express();
app.set("view-engine", "ejs");
app.use(bodyparser.urlencoded({ extended: true }));
app.use(express.json());

const url =
  "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/mailer?retryWrites=true&w=majority";

mongoose
  .connect(url, {
    useNewUrlParser: true,
  })
  .then(() => {
    console.log("connected to DB");
  });

cron.schedule("* 14  * * *", () => {
  function mailler(mail) {
    var mailOptions = {
      from: "www.sabarisekar@gmail.com",
      to: mail,
      subject: "sample",
      text: "test mail",
    };
    var transport = mailer.createTransport({
      service: "gmail",
      auth: {
        user: "www.sabarisekar@gmail.com",
        pass: "jxzkogacjgmtchxp",
      },
    });
    transport.sendMail(mailOptions, (err, info) => {
      if (err) {
        console.log(err);
      } else {
        console.log(info.response);
      }
    });
  }

  const data = model.find().then((data) => {
    console.log(data);
    Object.keys(data).forEach((i) => {
      mailler(data[i].mail);
      console.log(data[i].mail);
    });
  });
});
app.get("/signup", (req, res) => {
  res.render("signup.ejs");
});
app.post("/signup", (req, res) => {
  const data = new model({
    name: req.body.name,
    mail: req.body.email,
  });
  data.save().then((data) => {
    console.log(data);
  });
});
app.listen("8082");
