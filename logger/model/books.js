const mongoose = require('mongoose');

const booksSchema = mongoose.Schema({
    name: String,
    author: String,
    avail: Boolean
})

module.exports = mongoose.model('book',booksSchema);