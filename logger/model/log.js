const mongoose = require('mongoose');

const logSchema = mongoose.Schema({
    name: String,
    logIn: Number,
    logOut: Number,
    totalTime: Number,
    events: [String]
})

module.exports = mongoose.model('log',logSchema);