const express = require('express');
const bcrypt = require('bcrypt');
const app = express();
const jwt = require('jsonwebtoken');
const auth = require('../auth');
app.set('view-engine','ejs');

const User = require('../model/model');
const log = require('../model/log');
const book = require('../model/books');

var logs = []


const router = express.Router();

router.get('/signup',(req,res)=>{
    res.render('signup.ejs');
})

router.get('/login',(req,res)=>{
    res.render('login.ejs');
})

var login =  Date.now();
var logout = (Date.now()+500000)
// var entry = {
//     name: data.email,
//     logIn:login,
//     logOut:logout,
//     totalTime: logout-login,
//     events:[]
// }

router.delete('/logout',auth,(req,res)=>{
    logs[0].totalTime = Date.now()-(logs[0].logIn);
    logs[0].logOut = logs[0].logOut - logs[0].totalTime;
    console.log(logs[0].name);

    console.log(logs)
    const payload = {
        id: req.user.id
    }
    const entry = new log({
        name:logs[0].name,
        logIn: logs[0].logIn,
        logOut: logs[0].logOut,
        totalTime: logs[0].totalTime,
        events: logs[0].events
    });

    jwt.sign(payload,"secret",{ expiresIn: 1},(err,token)=>{
        if(err){
            res.send(err)
        }
        res.status(200).json({
            token
        });
        // var dec = jwt.verify(exp,"secret");
        // console.log(dec.id);
    })
   
    console.log(entry);
    entry.save()
    .then((data)=>{
        console.log(data)
    });
})

router.post('/register',async (req,res)=>{
    const email = req.body.email;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password,salt); 
    const user = new User({
        email: req.body.email,
        password: hashedPassword
    })
    try{
        User.findOne({email}).then((data)=>{
            return res.status(400).json({
                message: "user already exist"
            })
        })
      user.save()  
    }catch(err){
        console.log(err)
    }
})


router.post('/login',(req,res)=>{
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({email: email}).then(async(data)=>{
        console.log(data)
        const pass =  await bcrypt.compare(password, data.password);
        console.log(pass)
        var entry = {
            name: data.email,
            logIn:login,
            logOut:logout,
            totalTime: logout-login,
            events:[]
        }
        logs.push(entry);
        console.log(logs[0].events)
        console.log(logs)
        var date = new Date((Date.now()*1000))
        console.log(date);
        if(pass){
            const payload = {
                id: data.email
            }
            jwt.sign(payload,"secret",{ expiresIn: 500000},(err,token)=>{
                if(err){
                    res.send(err)
                }
                res.status(200).json({
                    token
                });
                // var dec = jwt.verify(exp,"secret");
                // console.log(dec.id);
            })
         
        }
    })
})
// router.get('/rent/:id',(req,res)=>{
//     var id = req.params.id
//     console.log(id)
//     res.redirect(301,'routes/rent/'+id);  
// });
router.post('/rent/:id',auth,(req,res)=>{
    var id = req.params.id;
    var msg = `${id} is taken`
    logs[0].events.push(msg);
    console.log(logs[0])
    book.findOneAndUpdate({name: id},{avail: false},null,(err,docs)=>{
        if(err){
            console.log(err);
        }
    })
});

router.post('/return/:id',auth,(req,res)=>{
    var id = req.params.id;
    book.findOneAndUpdate({name: id},{avail: true},null,(err,docs)=>{
        if(err){
            console.log(err);
        }
    })
});
// var entry = {
//     name: data.email,
//     logIn:login,
//     logOut:logout,
//     totalTime: logout-login,
//     events:[]
// }

router.get('/id',auth,(req,res)=>{
    console.log(req.user.id);
    const id = req.user.id;
    User.findOne({email: id})
    .then((data)=>{
        res.send(data);
    })
    
    
})

module.exports = router;


