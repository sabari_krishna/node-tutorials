const express = require('express');
const mongoose = require('mongoose');
const mongodb = require('mongodb');
const bodyparser = require('body-parser');
const router = require('./routes/routes');

const app = express()
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
    });

mongoose.Promise = global.Promise

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/CRUD?retryWrites=true&w=majority";
mongoose.connect(url,{
    useNewUrlParser: true
}).then(()=>{
    console.log("connected to db");
})
app.use('/routes',router);

app.listen(8080,()=>{
    console.log("listening at 8080......");
})