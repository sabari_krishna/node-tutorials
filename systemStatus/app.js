const express = require('express');
const mongoose = require('mongoose');
const router = require('./routes/routes')
const bodyparser = require('body-parser');


const app = express()
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
mongoose.Promise = global.Promise

// url for the database connection
const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/status?retryWrites=true&w=majority";
mongoose.connect(url,{
    useNewUrlParser: true
}).then(()=>{
    console.log("connected to db");
})
//ejs engine setup
app.set('view-engine','views')

//routing middleware
app.use('/routes',router);


//server creation
app.listen('8080',()=>{
    console.log("listening at 8080");
})
