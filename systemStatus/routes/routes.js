const express = require("express");
const model = require("../model/model"); //systemstatus model
const router = express.Router(); //router for the routing of api
const system = require("../model/system");
const mongoose = require("mongoose");

/* thiws api will show the list of system which is available*/
router.get("/status", (req, res) => {
  model.find().then((data) => {
    res.send(data);
  });
});

/*This api will render the addData.ejs file to the user in that page the user can 
add a new system to the cluster of system*/ 
router.get("/addsystem", (req, res) => {
  res.render("addData.ejs");
});

/*the addsystem get api will send the post request to the addsystem post api then 
the data will be validated and then they will saved in the database*/
router.post("/addsystem", (req, res) => {
  console.log(req.body);
  model.find().then((data) => {
    var no = Object.keys(data).length + 1;
    console.log(no);
    var status = new model({
      SystemNo: no,
      Name: req.body.sysName,
      Status: false,
    });
    var entry = new system({
      SystemNo: no,
      Name: req.body.sysName,
      Height: req.body.height,
    });
    entry.save();
    status.save().then((data) => {
      if (data) {
        res.send("System created");
      }
    });
  });
});

/*This api will render the index.ejs file where the user can select one of the system 
which has been created then the system data will get sent to next api which is "systemstatus"
to check whether the system is online or offline */
router.get("/systemlist", (req, res) => {
  model.find().then((data) => {
    res.render("index.ejs", { rows: data });
  });
});

/* "systemlist" api will send the request to this "systemstatus" api this will check the
status of the system and it shows the status of the system and allows the user to edit
 the status of the system */
router.post("/systemstatus", (req, res) => {
  // console.log(req.params);
  model.findOne({ Name: req.body.selectpicker }).then((data) => {
    // console.log(req.body)
    // console.log(data);
    res.render("status.ejs", { status: data.Status, name: data.Name });
  });
});

/*To update the height the height of the system the data need to be provided 
to get the data from the user the update.ejs file will be rendered when the "updatedata" 
api called.*/ 
router.get("/updatedata", (req, res) => {
  model.find().then((data) => {
    res.render("update.ejs", { rows: data });
  });
});

/*update api will check wheteher if that sysytem is online the data will be updated then 
the success response will get sent or if it is offline a failiure response will get sent */
router.post("/update", (req, res) => {
  model.findOne({ Name: req.body.selectpicker }).then((data) => {
    if (data.Status == false) {
      res.send("System is offline");
    } else {
      system.findOne({ Name: req.body.selectpicker }).then((data) => {
        // console.log(data);
        var entry = new system({
          SystemNo: data.SystemNo,
          Name: req.body.selectpicker,
          Height: req.body.height,
        });
        console.log(entry);
        entry.save().then((data) => {
          if (data) {
            res.send("Height updated");
          }
        });
      });
    }
  });
});

/** when the system status is changed the system status must be changed in thd db also so that
 * the changestatus will api will get trigerred
 */
router.post("/changestatus", (req, res) => {
  // console.log(req.body);
  var check;
  if (req.body.check == "on") {
    check = true;
  } else {
    check = false;
  }
  model
    .findOneAndUpdate({ Name: req.body.sysname }, { Status: check })
    .then((data) => {
      res.send("Updated");
    });
});

module.exports = router;
