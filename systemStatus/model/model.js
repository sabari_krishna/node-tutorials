const mongoose = require('mongoose'); //schema for system status

const systemSchema = mongoose.Schema({
    SystemNo: Number,
    Name:String,
    Status:Boolean
})

module.exports = mongoose.model('status',systemSchema);