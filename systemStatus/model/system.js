const mongoose = require('mongoose'); //schema for system data

const sysData = mongoose.Schema({
    SystemNo:Number,
    Name:String,
    Height:Number
})


module.exports = mongoose.model('sysData',sysData);