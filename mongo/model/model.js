const mongoose = require('mongoose');

const data = mongoose.Schema({
    id:String,
    model:String,
    type:String,
    serialNo: String,
    monior:{
        wlan0:{
            timeStamp:[Number],
            noOfClient:[Number],
            channelUtil:[Number],
            bandwidth:[Number]
        },
        wlan1:{
            timestamp:[Number],
            noOfClient:[Number],
            channelUtil:[Number],
            bandwidth:[Number]
        }
    }
})

module.exports = mongoose.model("data",data);