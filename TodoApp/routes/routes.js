const express = require('express');
const { route } = require('express/lib/application');
const res = require('express/lib/response');
const mysql = require('mysql');
const router = express.Router();
const config = require('../config/config');
const connction = mysql.createConnection(config);

router.get('/getAll',(req,res)=>{
    connction.query("select * from todolist",(err,result)=>{
        if(err) throw err;
        res.send(result);
    })
});

router.post("/addTask",(req,res)=>{
    const sqlQuery="insert into todolist (date,description) values ('" + req.body.date +"','"+req.body.desc+"')"
    connction.query(sqlQuery,(err,result)=>{
        if(err){
            console.log(err);
            res.send("Something went wrong")
        } 
        else{
            res.send({
                code:200,
                msg:"Task added successfully"
            });
        }
    });
})

router.put('/changeStatus',(req,res)=>{
    const sqlQuery = "update todolist set status = 'finished' where id=" + req.body.id;
    
    connction.query(sqlQuery,(err,result)=>{
        if(err){
            res.send("Something went wrong")
        }
        else{
            res.send({
                code:200,
                msg:"changed status successfully"
            })
        }
})
});

router.delete('/deleteTask',(req,res)=>{
    const sqlQuery = "delete from todolist where id=" + req.body.id;

    connction.query(sqlQuery,(err,result)=>{
        if(err){
            res.send("Somethimg went wrong")
        }
        else{
            res.send({
                code:200,
                msg:"Task deleted successfully"
            })
        }
    })
})



module.exports = router;