const express = require('express');
const mysql = require('mysql');
const bodyparser = require('body-parser');
const config = require('./config/config');
const routes = require('./routes/routes')
const { urlencoded } = require('body-parser');
const connection = mysql.createConnection(config);

const app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded(urlencoded({extended:true})));
app.use('/routes',routes);

const port = 8080;

connection.connect((err)=>{
    if(err){
        console.log(err)
    }
    console.log("Connected to DB");
});

var server =app.listen(port,()=>{
    console.log(`listening at ${port}`);
})
module.exports = server;