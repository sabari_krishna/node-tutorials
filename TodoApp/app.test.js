const request = require('supertest');
const app = require('./app');
const mysql = require('mysql');
const config = require('./config/config');

describe("todo",()=>{
    beforeAll(async ()=>{
        const connection = await mysql.createConnection(config);
    })
    it("Get list of todo data",()=>{
        return request(app)
        .get('/routes/getAll')
        .expect("Content-Type",/json/)
        .expect(200)
        .then((response)=>{
            expect(response.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        date:expect.any(String),
                        description:expect.any(String),
                        status:expect.any(String),
                        id:expect.any(Number)
                    }),
                ])
            );
        });
    });
    it("post task to db",()=>{
        return request(app)
        .post('/routes/addTask')
        .send({
            date:"15-05-2022",
            desc:"Bread"
        })
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            code:200,
            msg:"Task added successfully"     
        })
        );
      });
    });
    it("404 not found", () => {
        return request(app).get('/routes/todos').expect(404);
      });
    it("update value",()=>{
        return request(app)
        .put('/routes/changeStatus')
        .send({
            id:24
        })
        .expect(200)
        .then((response) => {
            expect(response.body).toEqual(
              expect.objectContaining({
                code:200,
                msg:"changed status successfully"     
            })
            );
          });
    })
});