// passport main engine

const LocalStratagy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

//initialize function is exported and used in app.js file

function initialize(passport, getUserByEmail, getUserById){  //getUserByEmail and getUserById are functions that will return a object
    const authenticateUser = async (email,password,done)=>{  //authenticate user is a callback function of passport.use function
        const user = getUserByEmail(email)
        if(user === null){
            return done(null,false,{message: 'User not found'}); //returns a message if the user doesn't exist
        }
        try{
            if(await bcrypt.compare(password,user.password)){
                return done(null,user) //returns the user to home page if the user exist and the password is correct
            }
            else{
                return done(null,false,{message: 'invalid password'}) //returns a message if the password is incorrect
            }
        }catch(err){
            return done(err); 
        }


    }
    passport.use(new LocalStratagy({usernameField: 'email'},authenticateUser)) //this will get called if passport.authenticate('local) is called
    //the username and password field must have the same name as username and pasword

    
    passport.serializeUser((user,done)=> done(null,user.id))
    passport.deserializeUser((id,done)=>{
        return done (null, getUserById(id))
    })

}
module.exports = initialize