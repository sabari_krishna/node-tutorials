//                                           Authentication without using DB                                    

//middleawares           
const express = require('express');
const bcrypt = require('bcrypt');
const app = express();
const passport = require('passport')
const initializePassport = require('./passport-config');
const flash = require('express-flash');
const session  =  require('express-session');
const methodOverride = require('method-override');

//  static array to store data 
const users = []


app.use(express.urlencoded({extended: false}));
app.use(express.json())


app.use(flash())
app.use(methodOverride('_method'));
app.use(session({secret: "secret"}))
app.set('view-engine','ejs');



//session  initialization
app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//      passport initializtion    
initializePassport(passport, 
    email => users.find(user => user.email === email),
    id => users.find(user => user.id === id)
    );


//routes          


//     logout route     
app.delete('/logout',(req,res)=>{
    req.logOut();
    res.redirect('/login');
})

//home page route
app.get('/',authenticated,(req,res)=>{              //this will get redirected only if the user is authenticated //
    res.render('home.ejs', { name: req.user.Name})
})

//  login route (get)  
app.get('/login',notAuthenticated,(req,res)=>{    
    res.render('login.ejs')
})

//     signup route(get)
app.get('/register',notAuthenticated,(req,res)=>{
    res.render('register.ejs')
});

//  login route (post)
app.post('/login',passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}))


//     signup route(post)
app.post('/register',notAuthenticated,async (req,res)=>{
    try{
        const hashedPassword = await bcrypt.hash(req.body.password,10); //the bcrypt middleware will change the password into a hash
        users.push({
            id: Date.now().toString(),
            email: req.body.email,
            Name: req.body.name,
            password: hashedPassword
        })
        console.log(users)
        
        res.redirect('/login');
    }catch{
        res.redirect('/register');
    }
})

//to check if the user is authenticated
function authenticated(req,res,next){
    if(req.isAuthenticated()){
        return next();      //if the user is authenticated the next function will get called
    }
    res.redirect('/login'); // if the user is not authenticated this function brings the user back to the login page
}
//too check if the user is not authenticated
function notAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return res.redirect('/') //if the user is authenticated the user will not be allowed to go back to login or register page without loging out
    }
    next();
}

app.listen('8080',()=>{
    console.log("listening at 8080");
})
