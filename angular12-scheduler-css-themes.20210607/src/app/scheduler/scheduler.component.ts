import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {DayPilot, DayPilotSchedulerComponent} from 'daypilot-pro-angular';
import {DataService} from './data.service'; {}

@Component({
  selector: 'scheduler-component',
  template: `
    <div class="space">
      Theme:
      <select [(ngModel)]="config.theme">
        <option *ngFor="let item of themes" [value]="item.value">{{item.name}}</option>
      </select>
      <label [hidden]="durationBarNotSupported">
      <input type="checkbox" [(ngModel)]="config.durationBarVisible">
        Show duration bar
      </label>
    </div>
    <daypilot-scheduler [config]="config" [events]="events" #scheduler></daypilot-scheduler>
  `,
  styles: [`
  label {
    margin-left: 20px;
  }
  `]
})
export class SchedulerComponent implements AfterViewInit {

  @ViewChild('scheduler')
  scheduler!: DayPilotSchedulerComponent;

  events: any[] = [];

  config: DayPilot.SchedulerConfig = {
    timeHeaders: [{groupBy:"Month"},{groupBy:"Day",format:"d"}],
    scale: "Day",
    treeEnabled: true,
    eventHeight: 40,
    days: DayPilot.Date.today().daysInYear(),
    startDate: DayPilot.Date.today().firstDayOfYear(),
    theme: "scheduler_default",
    durationBarVisible: true,
    onTimeRangeSelected: async args => {
      const dp = this.scheduler.control;
      const modal = await DayPilot.Modal.prompt("Create a new event:", "Event 1");
      dp.clearSelection();
      if (modal.canceled) { return; }
      dp.events.add({
        start: args.start,
        end: args.end,
        id: DayPilot.guid(),
        resource: args.resource,
        text: modal.result
      });
    }
  };

  get durationBarNotSupported(): boolean {
    return this.themes.find(item => item.value === this.config.theme).noDurationBarSupport;
  }

  themes: any[] = [
    {name: "Default", value: "scheduler_default"},
    {name: "Green", value: "scheduler_green"},
    {name: "Traditional", value: "scheduler_traditional"},
    {name: "Transparent", value: "scheduler_transparent"},
    {name: "White", value: "scheduler_white"},
    {name: "Theme 8", value: "scheduler_8", noDurationBarSupport: true}
  ];

  constructor(private ds: DataService) {
  }

  ngAfterViewInit(): void {
    this.ds.getResources().subscribe(result => this.config.resources = result);

    const from = this.scheduler.control.visibleStart();
    const to = this.scheduler.control.visibleEnd();
    this.ds.getEvents(from, to).subscribe(result => {
      this.events = result;
    });

    const thisMonth = DayPilot.Date.today().firstDayOfMonth();
    this.scheduler.control.scrollTo(thisMonth);
  }

}

