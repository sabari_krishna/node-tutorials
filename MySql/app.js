var express = require("express");
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.json());


const db = require("./config/db.config");


db.sequelize.sync().then(() => {
  console.log("Connected");
});

require("./routes/routes")(app);


var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Application request listening at http://%s:%s", host, port);
});
