const env = require('./env');

const Sequelize = require('sequelize');

const sequelize = new Sequelize(env.database,env.username,env.password,{
    host:env.host,
    dialect:env.dialect
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.book = require("../model/model")(sequelize,Sequelize);
db.customer = require("../model/customer")(sequelize,Sequelize);
 module.exports = db