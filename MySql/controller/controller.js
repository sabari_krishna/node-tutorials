
const db = require('../config/db.config');
const book = db.book;
const customer = db.customer;

exports.create = (req,res)=>{
    book.create({
        name:req.body.name,
        price:req.body.price,
        author:req.body.author,

    }).then((data)=>{
        console.log(data);
        res.send({
            code:200,
            data:data
        })
    })
    // console.log(req.body)
};



exports.createCustomer = (req,res)=>{
    customer.create({
        name:req.body.name,
        bookId:req.body.bookId,
        age:req.body.age,
        city:req.body.city,

    }).then((data)=>{
        console.log(data);
        res.send({
            code:200,
            data:data
        })
    })
    // console.log(req.body)
};

exports.findAll = (req,res)=>{
    // book.findAll().then((books)=>{
    //     console.log(books)
    //     res.send({
    //         code:200,
    //         data:books
    //     })
    // })
};

exports.update = (req,res)=>{
    if(typeof(req.body.id) == "string"){
        res.send({
            msg:"Invalid Id"
        })
    }
    else{
        const id = req.body.id;
        book.update(
            {
                name:req.body.name,
                price:req.body.price,
                author:req.body.author,
                available:req.body.available
            },
            {where:{id:req.body.id}}
        ).then((data)=>{
            res.send({
                code:200,
                data:data
            })
        })
    }
    
};

exports.delete = (req,res)=>{
    if(typeof(req.body.id) == "string"){
        res.send({
            msg:"Invalid Id"
        })
    }
    else{
        book.destroy(
            {where:{id:req.body.id}}
        ).then((data)=>{
            res.send({
                code:200,
                data:data
            })
        })
    }
    
}

