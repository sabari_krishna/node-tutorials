module.exports = function (app) {
  
    const book = require("../controller/controller");
  
    app.get("/api/inner")
    app.post("/api/book", book.create);

    app.post("/api/customer",book.createCustomer);

    app.get("/api/books", book.findAll);

    app.put('/api/update',book.update);

    app.delete('/api/delete',book.delete);

  };
  