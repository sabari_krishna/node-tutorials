
module.exports = (sequelize,Sequelize) =>{
    const Book = sequelize.define("book",{
        bookId:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true,
            
        },
        name:{
            type: Sequelize.STRING(100)
        },
        price:{
            type:Sequelize.INTEGER
        },
        author:{
            type:Sequelize.STRING(100)
        },
        available:{
            type:Sequelize.BOOLEAN,
            defaultValue:true
        }
        

    })
    return Book;
};
