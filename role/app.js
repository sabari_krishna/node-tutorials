const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const modal = require('./model/model');

const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

mongoose.Promise = global.Promise;

const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/Role?retryWrites=true&w=majority";

mongoose.connect(url,{
    useNewUrlParser:true
}).then(()=>{
    console.log("Connected to DB");
});

app.post('/register',(req,res)=>{
    // console.log(req.body)
    const data = new modal({
        Id:req.body.id,
        Name:req.body.name,
        Role:req.body.role
    })
    data.save().then((data)=>{
        console.log(data);
    })
    res.send("data saved");
})
app.get('/role',(req,res)=>{
    modal.findOne({Name: req.body.name})
    .then((data)=>{
        res.send(data.Role);
    })
})

app.listen('8080',()=>{
    console.log("listening at 8080");
})

