const bodyParser = require('body-parser');
const express = require('express');
const model = require('./model/model');
const mongoose = require('mongoose');
const {isAdmin,isStudent} = require('./middleware/auth');

mongoose.Promise = global.Promise;

const app = express();

const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/Role?retryWrites=true&w=majority";

mongoose.connect(url,{
    useNewUrlParser: true
}).then(()=>{
    console.log("Connected to DB");
})

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.get('/course',isStudent(["1","2"]),(req,res)=>{
    // console.log(data1);
    res.json({
        1:"CSE",
        2:"ECE",
        3:"IT"
    })
})

app.get('/course/1',isStudent(["1"]),(req,res)=>{
    res.json({
        1:"EEE",
        2:"EI"
    })
})

app.get('/course/2',isStudent(["2"]),(req,res)=>{
    res.json({
        1:"Mech",
        2:"Civil"
    })
})

app.get('/grades',isAdmin(["admin"]),(req,res)=>{
    res.json({
        1:"A",
        2:"A",
        3:"O"
    })
})
app.post('/register',(req,res)=>{
    const user = new model({
        Id:req.body.id,
        Name:req.body.name,
        Role:"Student",
        Sub:req.body.sub
    })
    user.save()
    .then((data)=>{
        console.log(data);
    })
})

app.listen('8080',()=>{
    console.log("listening at 8080");
})
