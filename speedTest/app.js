// const NetworkSpeed = require('network-speed');
// const testNetworkSpeed = new NetworkSpeed();

// getNetworkDownloadSpeed();

// async function getNetworkDownloadSpeed() {
//   const baseUrl = 'https://eu.httpbin.org/stream-bytes/500000';
//   const fileSizeInBytes = 500000;
//   const speed = await testNetworkSpeed.checkDownloadSpeed(baseUrl, fileSizeInBytes);
//   console.log(`Your Network speed is ${speed.mbps} Mbps`);
// }

const express = require("express");

const app = express();
const { exec } = require("child_process");

// Home Route
app.get("/", (req, res) => {
res.sendFile(__dirname + "/views/index.html");
});

// Speed Test
app.post("/test", (req, res) => {
exec("speed-test --json", (err, stdout, stderr) => {
	if (err || stderr) return res.send(
	"Error while testing internet speed.");
	const result = JSON.parse(stdout);
	const response = `<center>
					<h2>Ping : ${result.ping}</h2>
					<h2>Download Speed : ${result.download}</h2>
					<h2>Upload Speed : ${result.upload}</h2>
					</center>`;
	res.send(response);
});
});

// Server
app.listen(4000, () => {
console.log("Server running on port - 4000");
});



