const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser')
const routes = require('./routes/routes');
const app = express();
const cors = require('cors');

mongoose.Promise = global.Promise
const url = 'mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/LibraryManagement?retryWrites=true&w=majority'

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));
// app.use(cors())
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use("/images", express.static((__dirname+'/assets')));

mongoose.connect(url,{
    useNewUrlParser:true
}).then(()=>{
    console.log("connected");
})

app.use('/routes',routes);

app.listen('8080',()=>{
    console.log("Listening at 8080");
})