const express = require('express');
const router = express.Router();
const admin = require('../model/admin');
const employee = require('../model/employee');
const book = require('../model/books');
const books = require('../model/books');
const nodemailer = require('nodemailer')
const res = require('express/lib/response');
const borrowed = require('../model/borrowed');
const req = require('express/lib/request');
const multer = require('multer')

//duedate calculation
var dueDate = new Date();   
dueDate.setDate(dueDate.getDate() +30);
dueDate = dueDate.toLocaleDateString();

const fileStorage = multer.diskStorage({
    destination:(req,file,cb)=>{
        console.log(file.mimetype)
        // console.log(__dirname)
        cb(null,'/home/vvdn/Desktop/Node/libmngmtsys/assets')
    },
    filename:(req,file,cb)=>{
        cb(null,file.originalname)
    }
})
const upload = multer({storage:fileStorage});





//email 
function mailer(mail,sub,content){
    var mailOptions = {
        from: "www.sabarisekar@gmail.com",
        to: mail,
        subject: sub,
        text: content,
      };
      var transport = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "www.sabarisekar@gmail.com",
          pass: "jxzkogacjgmtchxp",
        },
      });
      transport.sendMail(mailOptions, (err, info) => {
        if (err) {
          console.log(err);
        } else {
          console.log(info.response);
        }
      });

}

//LogIn API
router.post('/login',(req,res)=>{
    console.log(req.body)
    admin.findOne({Name:req.body.userName})
    .then((data)=>{
        if(data == null){
            employee.findOne({Name:req.body.userName})
            .then((data)=>{
                if(data ==  null){
                    res.send({
                        code:404,
                        msg:"Login Failed"
                    })
                }
                else if(data.Name == req.body.userName){
                    res.send({
                        code:200,
                        msg:"Login Successfull",
                        userData:data,
                        role:"employee"
                    })
                }
            })
        }
        else{
            if(data.Name == req.body.userName){
                res.send({
                    code:200,
                    msg:"Login Successfull",
                    userData:data,
                    role:"admin"
                })
            }
          
        }
    })
    .catch((err)=>{
        console.log(err)
        res.send({
            code:404,
            msg:"Login Failed"
        })
    })
})


//return book API 
router.delete('/return/:bookId',(req,res)=>{
    borrowed.findOneAndDelete({BookId:req.params.bookId})
    .then((data)=>{
        console.log(data)
        book.findOneAndUpdate({BookId:req.params.bookId},{
            $set:{
                Status:"Available"
            }
        })
        .then((data)=>{
            res.send(data)
            
        })
        .catch((err)=>{
            res.send("Unable to return the book")
        })
    })
})  


router.post('/mail',(req,res)=>{
    mailer(req.body.To,req.body.Subject,req.body.Content);    
})

//API to add book to the library
router.post('/addBook',(req,res)=>{
    // console.log(__dirname)
    console.log(req.body)
    var newBook = new book({
        BookId:req.body.BookId,
        BookName:req.body.BookName,
        Status:"Available",
        ReviewLink:req.body.Youtube, 
        Location:req.body.Location,
        
    })
    newBook.save()
    .then((data)=>{
        res.send(data)
        console.log(data)
    })
    .catch((err)=>{
        res.send("Unable to add book");
    })

})

//show all the books in the db
router.get('/allBooks',(req,res)=>{
    books.find({},{_id:0,__v:0})
    .then((data)=>{
        res.send({
            data
        })
    })
    .catch((err)=>{
        res.send("Unable to get books")
    })
})


//api to add book to wishlist wishlist 
router.put('/wishlist',(req,res)=>{
    console.log(req.body)
    employee.findOneAndUpdate({Id:req.body.empId},{
        $push:{Wishlist:{
            BookId:req.body.book.BookId,
            BookName:req.body.book.BookName
        }}
    }).then((data)=>{
        res.send({
            code:200,
            msg:"Success"
        })
    })
    .catch((err)=>{
        res.send("Unexpected Error")
    })
    
})

router.get('/wishlist/:empId',(req,res)=>{
    console.log(req.params)
    employee.findOne({Id:req.params.empId})
    .then((data)=>{
        res.send(data.Wishlist)
    })
})


//show borrowed books of a particular employee
router.get('/borrowedBooks/:empId',(req,res)=>{
    console.log(req.params)
    borrowed.find({UserId:req.params.empId})
    .then((data)=>{
        res.send(data)
    })
    .catch((err)=>{
        res.send("Unable to get books")
    })    

})

//get all borrowed book and display it to admin
router.get('/borrowedBooks',(req,res)=>{
    borrowed.find({Status:"Accepted"})
    .then((data)=>{
        res.send(data);
        console.log(data)
    })
    .catch((err)=>{
        res.send("Unable to get books")
    })
})

//to borrow a book from the library
router.put('/borrowBook',(req,res)=>{
    console.log(req.body)
            book.findOneAndUpdate(
                {BookId:req.body.id.BookId},
                {$set:{
                    Status:"Not Available"
                }}
            )
            .then((data)=>{
                console.log(data)
                var entry = new borrowed({
                    UserId:req.body.user.id,
                    UserName:req.body.user.name,
                    BookId:req.body.id.BookId,
                    BookName:req.body.id.BookName,
                    Status:"Pending",
                    Location:req.body.id.Location,
                    DueDate:dueDate,
                    IssuedDate:(new Date()).toLocaleDateString(),
                })
                entry.save()
                .then((data)=>{
                    console.log(data)
                })

            })
        
})

router.get('/request',(req,res)=>{
    borrowed.find({Status:"Pending"})
    .then((data)=>{
        res.send(data)
    })
})
router.put('/request',(req,res)=>{
    console.log(req.body)
    // console.log("return")
    borrowed.findOneAndUpdate({BookId:req.body.data.BookId},{
        $set:{
            Status:req.body.Status
        }
    }).then((data)=>{
        res.send(data)
        if(req.body.Status == "Rejected"){
            book.findOneAndUpdate({BookId:req.body.data.BookId},{$set:{
                Status:"Available"
            }}).then((data)=>{
                console.log(data)
            })
            
        }
    })
})


//API to get all books based on the location
router.get('/allBooks/:location',(req,res)=>{
    
    book.find({Location:req.params.location})
    .then((data)=>{
        
        res.send({data})
    })
    .catch((err)=>{
        res.send("Unable to get books")
    })
})


// API to delete a book from the library
router.delete('/delete/:bookId',(req,res)=>{
    console.log(req.params)
    books.findOneAndDelete({BookId:req.params.bookId})
    .then((data)=>{
        res.send(data)
        console.log(data)
    })
    .catch((err)=>{
        res.send("Unable to delete book")
    })
})

//edit book data 
router.put('/edit',(req,res)=>{
    console.log(req.body)
    console.log(req.body)
    // console.log(req.body.data.BookId)
    books.findOneAndUpdate({BookId:req.body.data.BookId},
        {
            $set:{
                BookName:req.body.data.BookName,
                Location:req.body.data.Location,
            }
                
        }).then((data)=>{
            console.log(data)
            res.send({
                code:200,
                msg:"Data Changed"
            })
        })
})

module.exports = router






// upload.single("file"),
// BookImage:"/images/"+req.file.filename,  