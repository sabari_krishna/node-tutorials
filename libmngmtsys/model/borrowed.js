const mongoose = require('mongoose');

const borrowed = mongoose.Schema({
    UserId:String,
    UserName:String,
    BookId:String,
    BookName:String,
    Status:String,
    Location:String,
    DueDate:String,
    IssuedDate:String
})

module.exports = mongoose.model('borrowed',borrowed);