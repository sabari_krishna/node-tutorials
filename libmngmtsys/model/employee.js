const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    Name:String,
    Id:String,
    Age:Number,
    Password:String,
    Wishlist:[{
        BookId:String,
        BookName:String
    }]

})
module.exports = mongoose.model('employee',employeeSchema);