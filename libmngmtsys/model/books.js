const mongoose = require('mongoose');

const bookSchema = mongoose.Schema(
    {
        BookId:String,
        BookName:String,
        BookImage:String,
        ReviewLink:String,
        Location:String,
        Description:String,
        Status:{type:String,default:"Available"},

        

    }
)

module.exports = mongoose.model('book',bookSchema);