const mongoose = require('mongoose');

//creating a structure for the DB
const CrudSchema = mongoose.Schema({
    id: Number,
    mail: String,
    password:String
});

//exporting the model
module.exports = mongoose.model("app",CrudSchema);