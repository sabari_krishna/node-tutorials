//routes

const App = require('../model/model');
const express = require('express');
const router = express.Router();
var len = 0;

//router get method to get all the data from the database
    router.get('/all',(req,res)=>{    // 
        App.find() //funtion to show every data
        .then((data)=>{
            console.log(data);
            res.send(data); //show the data
            len = Object.keys(data).length;
            // console.log(len)
        });
        // console.log(len)
    });

    // //get method to show only the desired data from the database
    //     router.get('/findone',(req,res)=>{ 
    //         App.findOne({ id:req.body.id}) //funtion to search and find for a specific data
    //         .then((data)=>{
    //             console.log(req.body.id);
    //             console.log(data)
    //             if(!data){
    //                 return res.status(404).send({
    //                     message: "message not found" //if the message is not available in db
    //                 });
    //             }
    //             res.send(data); //show if the message is available
    //         });
    //     });

//post method to create a new data and save it in the database
    router.post('/create',(req,res)=>{
        console.log("working....")
        console.log(req.body)
        const message = new App({
            id:req.body.id,
            mail: req.body.email, //message from the body   
            password: req.body.password  
        });
        message.save() //function to save in the database 
        .then((data)=>{
            res.send(data);
        });
    });
    

    module.exports = router;