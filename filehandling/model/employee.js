const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({  //schema for employee
    DeptId:Number,
    EmpId:Number,
    Name:String,
    Bonus:Number
})

module.exports = mongoose.model('empData',employeeSchema);