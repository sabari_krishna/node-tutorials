//fileHandling using Multer
const express = require("express");
const multer = require("multer");               //multer forfile handling
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const model = require("./model/model");         //db model for file path upload
const employee = require("./model/employee");   // db model for employee data
const reader = require("xlsx");                 //xlsx middleware for handling xlsx files

mongoose.Promise = global.Promise;

const url = "mongodb+srv://sabari:sabari@cluster0.ts2p0.mongodb.net/multer?retryWrites=true&w=majority"; //url for the connection for the database

mongoose                      //DB connection 
  .connect(url, {                   
    useNewUrlParser: true,        
  })                              
  .then(() => {                   
    console.log("connected to DB"); 
  });

const app = express(); //express application
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use("/images", express.static("images"));  //to access the images from the server side 
app.set("view-engine", "ejs");   //ejs engine setup

const fileStorage = multer.diskStorage({     //multer storage variable
  destination: (req, file, cb) => {          //destination path for the file to be stored
    console.log(file.mimetype);
    if (
      file.mimetype ===
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"    //store in csv folder if it's in xlsx format
    ) {
      cb(null, "csv");  //callback function with no error and the path for the csv forder location
    }
    if (file.mimetype === "image/png") {      //store in images folder if it is png or jpeg fromat 
      cb(null, "images");  //call back function with no error and the path for images folder location 
    }
  },
  filename: (req, file, cb) => {  //name for the file to be stored
    cb(null, Date.now() + "-" + file.originalname);  
  },
});

const upload = multer({ storage: fileStorage });  //upload function 

function updateData(id, data) {  //update function for the db query
  employee
    .findOneAndUpdate(
      { EmpId: id },
      {
        DeptId: data.DeptId,
        Name: data.Name,
        Bonus: data.Bonus,
      }
    )
    .then((data) => {
      console.log(data);
    });
}

app.get("/", (req, res) => {  //get methd to render the index page to upload the file
  res.render("index.ejs");
});

app.post("/upload", upload.single("image"), (req, res) => {  //upload method to  upload the file it may be  ` either jpeg file or xlsx file
  if (req.file) {               //this api will save the url of the file in db 
    var emp = new model({
      path: req.file.path,      //url for the file
    });
    emp.save().then((data) => {     //save the data in the db
      console.log(data);
    }); 
  }
  console.log(req.file);
  res.send("Uploaded");
});
app.post("/uploadxlsx",upload.single("xlsx"), (req, res) => {        //upload method to  upload the xlsx file it can only be the xlsx file
  if (req.file) {     //this api will save the url of the file in db 
    var emp = new model({
      path: req.file.path,            //url for the xlsx file
    });
    emp.save().then((data) => {       //save the data in the db
      console.log(data);
    });
  }
  console.log(req.file);  

  const file = reader.readFile("./" + req.file.path);   //this api will read the data from the xlsx file and upload it to the db with the help of the headers
  const sheets = file.SheetNames;             
  for (let i = 0; i < sheets.length; i++) { //parsing through the sheets in the xlsx file
    // console.log(file.SheetNames[i]);
    const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]]);   //turn the data from file into json format

    temp.forEach((res) => {
      //   console.log(res.DEPTID);
      var data = new employee({
        DeptId: res.DeptID,
        EmpId: res.EmpID,
        Name: res.Name,
        Bonus: res.Bonus,
      });
      data.save().then((data) => {        //saving the data in db
        console.log(data);
      });
    });
  }
  res.send("Uploaded");
});

app.post("/updatedata", upload.single("xlsx"), (req, res) => {   //update the data in db by updating the xlsx file
  if (req.file) {
    var emp = new model({
      path: req.file.path,           //url for the xlsx file
    });
    emp.save().then((data) => {
      console.log(data);
    });
  }
  console.log(req.file);

  const file = reader.readFile("./" + req.file.path);   // read the xlsx file with the help of reader function
  const sheets = file.SheetNames;
  for (let i = 0; i < sheets.length; i++) { //loop for parsing through the file sheets
    console.log(file.SheetNames[i]);
    const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]]); //turn the data from file into json format
    temp.forEach((res) => {
      employee
        .findOne(
          { DeptId: res.DeptId },
          { _id: 0, DeptId: 1, EmpId: 1, Name: 1, Bonus: 1 }  //finding the employee details  
        )
        .then((data) => {
          //   console.log(data)
          //   console.log(res)
          if (                            //check weather the data has any changes
            data.DeptId != res.DeptId ||
            data.Name != res.Name ||
            data.Bonus != res.Bonus
          ) {
            updateData(res.EmpId, res);   //if there is any changes the data will be updated with the help of updateData function
          }
        });

      //   data.save().then((data)=>{
      //       console.log(data);
      //   })
    });
  }
  res.send("Uploaded");
});

app.listen("8081", () => {
  console.log("8080");
});
