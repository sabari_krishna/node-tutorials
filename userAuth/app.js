const express = require('express');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const session = require('express-session');
const initializePassport = require('./passport/passport-config');
const bodyParser = require('body-parser');
const router = require('./routes/routes');

// const LocalStrategy = require('./passport/passport);


const app = express();
// app.use(express.bodyParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
const MongoStore = require('connect-mongo');

app.use('/routes',router);

const url = "mongodb+srv://sabari:sabari@cluster0.m7la9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

app.use(session({
    secret: "neko",
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({
        mongoUrl: url
    }),
    cookie:{
        maxAge: 1000*60*60*24
    }
}));


app.use(passport.initialize());
app.use(passport.session());
initializePassport(passport);

app.post('/login',passport.authenticate('local'));
app.listen(8080,()=>{
    console.log("listening..")
})