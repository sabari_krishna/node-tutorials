const mongoose = require('mongoose');

const url ="mongodb+srv://sabari:sabari@cluster0.m7la9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

mongoose.connect(url,{
    useNewUrlParser: true
}).then(()=>{
    console.log("connected to db");
})

const user = mongoose.Schema({
    userid: Number,
    Name: String,
    hash: String,
    salt: String,

});

module.exports = mongoose.connection.model('user',user);
