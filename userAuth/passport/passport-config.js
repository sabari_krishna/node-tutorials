const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const connection = require('../model/model');
// const User = connection.models.User;
const validatePassword = require('../validate/validate').validatePassword;

const customFields = {
    userNameField:'uname',
    passwordField: 'pwd'
}


function initialize(passport){
    const verifyCallBack = (username, password , done)=>{
        console.log("lojk")
        connection.findOne({Name: username})
        .then((data)=>{
            if(!data){
                return(null,false);
            }
            const isValid = validatePassword(password, data.hash, data.salt);
    
            if(isValid){
                return done(null,data);
            }
            else{
                return done(null,false);
            }
        });
    }
    passport.use(new LocalStrategy(customFields,verifyCallBack));

    passport.serializeUser((user,done)=>{
        done(null,user.Name);
    })
    passport.deserializeUser((userId,done)=>{
        connection.findById(userId)
        .then((data)=>{
            done(null,data)
        }).catch(err=> done(err))
    })
}
module.exports = initialize

// passport.use(new LocalStrategy(function(username,password,done){
//     console.log("....")
//     connection.findOne({Name: username})
//     .then((data)=>{
//         if(!data){
//             return(null,false);
//         }
//         const isValid = validatePassword(password, data.hash, data.salt);

//         if(isValid){
//             return done(null,data);
//         }
//         else{
//             return done(null,false);
//         }
//     });

// }));




// const verifyCallBack = (username, password , done)=>{
//     console.log("lojk")
//     connection.findOne({Name: username})
//     .then((data)=>{
//         if(!data){
//             return(null,false);
//         }
//         const isValid = validatePassword(password, data.hash, data.salt);

//         if(isValid){
//             return done(null,data);
//         }
//         else{
//             return done(null,false);
//         }
//     });
// }
