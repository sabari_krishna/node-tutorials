const express = require('express');
const passport = require('passport');
const connection = require('../model/model');
const validate = require('../validate/validate');

const router = express.Router();


router.post('/login',passport.authenticate('local'));

router.post('/register',(req,res,next)=>{
    const saltHash = validate.generatePassword(req.body.pwd);

    const hash = saltHash.hash;
    const salt = saltHash.salt;
    

    console.log(req.body)

    const newUser = new connection({
        Name: req.body.uname,
        hash: hash,
        salt: salt
    });
    newUser.save()
    .then((data)=>{
        res.send(data);
    })
});

module.exports = router;